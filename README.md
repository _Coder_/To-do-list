# TO - DO LIST CHROME EXTENSION
A to-do list extension - a minimalist tool that helps you manage your daily tasks and stay organized. 

# Synopsis
- An extension that lives on a New Tab. 
- Uses a popup window.
- Easy to add, edit, check and delete items in your ToDo list.
- Can re-order items.
- Progress-bar for tasks completed.
- Clear completed items or delete the list with the delete button in the bottom right.

# Project Structure
- `src` folder contains everything other than boilerplate code.
- `manifest.json` file contains the manifest fields and their attributes required for the extension.
- `index.html` is the default popup of chrome extension.
- `static` folder contains static assets like css and js files.
- `app.js` contains the root component, it renders the TodoList component.
- `components` contains the TodoList component with all the logic for adding and removing the task from the list


# Getting Started
These instructions will get you a copy of the project up and running on your local machine.

- Clone the repository into your local machine.
- Install the dependencies using command `npm install`
- Run `npm run watch` to automatically track changes to the files.
- Open `chrome://extensions/` in the chrome browser.
- Enable developer mode by clicking on `Developer mode`.
- Click on `Load unpacked extension`.
- Select the `build` folder.
- The extension will be added to chrome and an icon will appear on the chrome menu.
- Click on the icon next to the url section and it should render the popup.

